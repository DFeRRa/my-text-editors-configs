set nocompatible
filetype plugin on
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
execute "set t_8f=\e[38;2;%lu;%lu;%lum"
execute "set t_8b=\e[48;2;%lu;%lu;%lum"
" set softtabstop=4
set t_Co=256
set t_ut=
set autoindent
let python_highlight_all = 1
set termguicolors
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
set mousehide
set mouse=a
set termencoding=utf-8
set novisualbell
set t_vb=
set backspace=indent,eol,start whichwrap+=<,>,[,]
set showtabline=1
set wrap
set linebreak
set nobackup
set noswapfile
set encoding=utf-8 " Кодировка файлов по умолчанию
set fileencodings=utf8,cp1251
set clipboard=unnamedplus
set ruler
let maplocalleader = "\\"
syntax enable
set hidden

autocmd InsertLeave * :normal `^
set virtualedit=onemore
set selection=exclusive
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprev<CR>
if has('unix')
    nnoremap d "0d
    nnoremap x "+x
    nnoremap p "+p
    nnoremap y "+y
    vnoremap d "0d
    vnoremap x "+x
    vnoremap p "+p
    vnoremap y "+y
endif
" set visualbell=
call plug#begin('~/.vim/plugged')
Plug 'junegunn/vim-easy-align'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'drmingdrmer/vim-tabbar'
Plug 'Disinterpreter/vim-pawn'
Plug 'freitass/todo.txt-vim'
Plug 'noahfrederick/vim-composer'
Plug 'noahfrederick/vim-laravel'
Plug 'jwalton512/vim-blade'
" Plug 'jacoborus/tender.vim'
Plug 'sainnhe/sonokai'
Plug 'jceb/vim-orgmode'
Plug 'tpope/vim-speeddating'
call plug#end()

colorscheme sonokai
" set makeprg=pawncc\ %:r.pwn\ -i\/d/ProgramFiles/pawno/include
" " function PawnMake()
" " silent make | copen
" " redraw!
" " endfunction
" " map <F5> :exec PawnMake()<CR>
