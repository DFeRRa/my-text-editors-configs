(add-to-list 'load-path (expand-file-name "~/"))
(require 'init)
(provide 'emacs)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(fish-mode web-server websocket markdown-preview-mode markdown-mode web-beautify json-mode yaml-mode window-number web-mode tabbar s reverse-im rainbow-mode mark-multiple indent-guide emmet-mode doom-themes dash)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
